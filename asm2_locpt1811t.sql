USE [master]
GO
/****** Object:  Database [asm2_locpt1811]    Script Date: 18/09/2023 16:45:02 ******/
CREATE DATABASE [asm2_locpt1811]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'asm2_locpt1811', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\asm2_locpt1811.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'asm2_locpt1811_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\asm2_locpt1811_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [asm2_locpt1811] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [asm2_locpt1811].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [asm2_locpt1811] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET ARITHABORT OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [asm2_locpt1811] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [asm2_locpt1811] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [asm2_locpt1811] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET  ENABLE_BROKER 
GO
ALTER DATABASE [asm2_locpt1811] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [asm2_locpt1811] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [asm2_locpt1811] SET  MULTI_USER 
GO
ALTER DATABASE [asm2_locpt1811] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [asm2_locpt1811] SET DB_CHAINING OFF 
GO
ALTER DATABASE [asm2_locpt1811] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [asm2_locpt1811] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [asm2_locpt1811] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [asm2_locpt1811] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [asm2_locpt1811] SET QUERY_STORE = OFF
GO
USE [asm2_locpt1811]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 18/09/2023 16:45:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Branch] ON 

INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (2, N'Chi Nhánh Trung Tâm', N'456 Ðu?ng Trung Tâm', N'H? Chí Minh', N'Active', N'90001')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (3, N'Chi Nhánh B?c', N'789 Ðu?ng B?c', N'H?i Phòng', N'DeActive', N'60601')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (4, N'Chi Nhánh Ðông', N'101 Ðu?ng Ðông', N'Ðà N?ng', N'Active', N'70001')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (5, N'Chi Nhánh Tây', N'202 Ðu?ng Tây', N'C?n Tho', N'Active', N'80001')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (6, N'Chi Nhánh Nam', N'303 Ðu?ng Nam', N'Nha Trang', N'DeActive', N'90001')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (15, N'string', N'string', N'string', N'active', N'string')
SET IDENTITY_INSERT [dbo].[Branch] OFF
GO
ALTER TABLE [dbo].[Branch]  WITH CHECK ADD CHECK  (([State]='DeActive' OR [State]='Active'))
GO
USE [master]
GO
ALTER DATABASE [asm2_locpt1811] SET  READ_WRITE 
GO
