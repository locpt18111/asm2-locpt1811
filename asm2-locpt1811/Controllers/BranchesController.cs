﻿using asm2_locpt1811.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace asm2_locpt1811.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BranchesController : ControllerBase
    {
        private readonly Asm2Locpt1811Context _context;
        public BranchesController(Asm2Locpt1811Context context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var branches = _context.Branches.ToList();
                if (branches.Count == 0)
                {
                    return NotFound("Branches not available");
                }
                return Ok(branches);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public IActionResult Get (int id)
        {
            try
            {
                var branches = _context.Branches.Find(id);
                if (branches == null)
                {
                    return NotFound($"Branches not found with {id}");
                }
                return Ok(branches);
            }catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Post(Branch model)
        {
            try
            {
                _context.Add(model);
                _context.SaveChanges();
                return Ok("Branch created");
            }
            catch (Exception ex)
            {
                return BadRequest($"Error: {ex.Message} | Inner Exception: {ex.InnerException?.Message}");
            }

        }

        [HttpPut]
        public IActionResult Put(Branch model)
        {
            try
            {
                if(model == null || model.BranchId == 0)
                {
                    if(model == null)
                    {
                        return BadRequest("Model data is not valid");
                    }
                    else
                    {
                        return BadRequest($"Branch id {model.BranchId} is not available ");
                    }
                }
                var branches = _context.Branches.Find(model.BranchId);
                if(branches == null)
                {
                    return BadRequest($"Branch id {model.BranchId} is not found ");
                }
                branches.State = model.State;
                branches.Address = model.Address;
                branches.Name = model.Name;
                branches.ZipCode = model.ZipCode;
                branches.City = model.City;
                _context.SaveChanges();
                return Ok("Branches change successfully");
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                var branch = _context.Branches.Find(id);
                if(branch == null)
                {
                    return BadRequest($"Branch id {id} is not found ");
                }
                _context.Remove(branch);
                _context.SaveChanges();
                return Ok("Branch remove successfully");
            }catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
